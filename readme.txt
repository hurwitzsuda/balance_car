 一、Hardward 			   硬件相关
1、Mechanical structure 		机械结构，solidworks2020
2、Circuit board 			电路板设计图，AD21

二、Software 				   软件相关
1、Balance_Car			    FreeRTOS版本源码，结构清晰，代码注释全
2、Balance_Car（无OS）		  无FreeRTOS版本源码，效果较差，不建议使用
3、Balance_Car Controller 	微信小程序源码


仅供交流学习使用。

2022/12/12
更新底板加工件DWG格式，方便加工制造。